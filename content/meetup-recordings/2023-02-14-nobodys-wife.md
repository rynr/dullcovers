---
title:         Anouk - Nobody's Wife
date:          2023-01-21
publishDate:   2023-01-20T20:00:00+0200
recordingDate: 2023-02-14T19:30:00+0200
categories:    meetup live-recording
audioDbId:     32795638
status:        Mixing
meetup:        https://www.meetup.com/musicloversmeetup-munich/events/291089369/
youtube:       oeoZ892ACC4
soundcloud:    1456120414
artists:
- voice: Vocals
  name: Cath
- voice: Acoustic Guitar
  name: Rohith
- voice: Rhythm Guitar
  name: Rohit
- voice: Bass
  name: Rainer
- voice: Drums
- name: Raoul
---
I didn't find enough time for a better video, but here it is.