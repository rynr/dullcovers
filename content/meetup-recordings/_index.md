---
title: Meetup-Recordings
date:  2022-07-14 21:53:43 +0200
outputs:
- html
- calendar
aliases:
- /faq.html
---
So far, these recordings happen in scope of the [Amateur Musicians Munich][amm].

The meetup runs like this:

- Setup the environment
- Rehearse the song
- Record the song
- Retrospective
- Decide on the next song for the next time

Hardware available:

- Microphones (2x [SM58][sm58])
- Drumset
- Electric Piano
- Guitar-Amp ([Marshall][mg101fx])
- Bass-Amp ([Ashdown][toneman300])
- Mixer ([QU-16][qu16])
- Monitor Speaker

### Are you interested to join?

Checkout the [Amateur Musicians Munich][amm] meetup group for events
named *Recording Session*. You can also subscribe to the [rss-feed][rss]
or [online-calendar][ics].

### Recordings so far

{{< youtube-playlist "PLYA18PjJlxqkA14BwJDOrhf3xHz3HYyhb" >}}

[rss]: https://dullcovers.eu/index.xml
[ics]: https://dullcovers.eu/meetup-recordings/index.ics
[amm]: https://www.meetup.com/musicloversmeetup-munich/
[qu16]: https://www.allen-heath.com/qu-series/qu-16/
[sm58]: https://www.shure.com/en-US/products/microphones/sm58
[toneman300]: https://ashdownmusic.com/products/mag-c115-300-evo-iii-combo
[mg101fx]: https://marshall.com/amps/products/archive/mg-gold/mg101fx