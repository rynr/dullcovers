---
title:         Sultans of Swing - Dire Straits
date:          2022-09-14
publishDate:   2022-09-14T00:00:00+0200
recordingDate: 2022-10-11T19:30:00+0200
categories:    meetup live-recording
audioDbId:     32856639
status:        Done
meetup:        https://www.meetup.com/musicloversmeetup-munich/events/288750616/
youtube:       sKUldzo_DSg
soundcloud:    1372480426
artists:
- voice: Vocals & Lead Guitar
  name: Rohith
- voice: Bass
  name: Rainer
- voice: Drums
  name: Fabian
---
## Video

Again some problems with the video, the camera stopped recording after the
second take, so I had to sync a different take video to the final mix.

Nevertheless, I think it's a good recording, have fun.

Please join our [next recording][next] where we will record a christmas song.

[next]: /meetup-recordings/2022-11-08-merry-christmas-everyone/
