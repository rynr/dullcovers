---
title:         Twistin' The Night Away - Sam Cooke
date:          2022-11-24
publishDate:   2022-11-24T19:00:00+0200
recordingDate: 2022-12-13T19:30:00+0200
categories:    meetup live-recording
audioDbId:     32754840
youtube:       pHHz89CIUcg
soundcloud:    1405341205
status:        Done
meetup:        https://www.meetup.com/de-DE/musicloversmeetup-munich/events/289852451/
artists:
- voice: Vocals
  name: Kristin
- voice: Piano
  name: Olga
- voice: Guitar
  name: Phillip
- voice: Bass
  name: Rainer
---
A classic.

We didn't have a drummer, but we managed to record with backing tracks.
