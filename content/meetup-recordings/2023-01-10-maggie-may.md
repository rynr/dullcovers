---
title:         Maggie May - Ros Steward
date:          2022-12-15
publishDate:   2022-12-15T20:00:00+0200
recordingDate: 2023-01-10T19:30:00+0200
categories:    meetup live-recording
audioDbId:     33824836
status:        Done
meetup:        https://www.meetup.com/musicloversmeetup-munich/events/290351692/
youtube:       AobWBKMium4
soundcloud:    1426106923
artists:
- voice: Vocals
  name: Cath
- voice: Guitar
  name: Dave
- voice: Piano
  name: Olga
- voice: Bass
  name: Rainer
- voice: Drums
  name: Fabian
---
A new song, again a classic.  
