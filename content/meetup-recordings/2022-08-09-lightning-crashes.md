---
layout:        meetup-recording
title:         Lightning Crashes - Live
date:          2022-08-09
publishDate:   2022-07-19T00:00:00+0200
recordingDate: 2022-08-09T19:30:00+0200
categories:    meetup live-recording
audioDbId:     32861318
soundcloud:    1325209342
youtube:       ZpJm_xQSVSA
status:        Done
meetup:        https://www.meetup.com/de-DE/musicloversmeetup-munich/events/287204445/
artists:
- voice: Vocals
  name: David
- voice: Guitar
  name: Rohith
- voice: Bass
  name: Rainer
aliases:
  - /recordings/2022-08-09-lightning-crashes
---
This was the second recording. Mixing and the Video are done. It looks ike we have more people for the next recording, I'm looking forward to it.

### Retrospective

- Getting sound from the PC does not work (would've been required to record with click and use multiple parallel recordings).
- The new camera setup is good.
- We should think about lighting.
- FX Setup for vocals was still a hazzle.
- Remote access to the mixer did not work (again), but as we did not use monitoring we did not really require it this time.
- We need to fine more people.
- Not as many drings are required (less to carry for the next time).

### Song/Ideas for the next round

- [**Bryan Adams - Heaven**][bah] ([recording](/meetup-recordings/2022-09-13-heaven/))

[bah]: https://www.youtube.com/watch?v=vf3zFvcKQkw
[ugtab]: https://tabs.ultimate-guitar.com/tab/live/lightning-crashes-guitar-pro-783438
[songsterr]: https://www.songsterr.com/a/wsa/live-lightning-crashes-bass-tab-s11538
[fmtab]: https://freemidi.org/artist-685-live
