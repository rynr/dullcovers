---
layout:        meetup-recording
title:         Rocking All Over The World!
date:          2022-07-12
publishDate:   2022-07-12T00:00:00+0200
recordingDate: 2022-07-12T19:30:00+0200
categories:    meetup live-recording
audioDbId:     34111888
meetup:        https://www.meetup.com/de-DE/musicloversmeetup-munich/events/286894214/
youtube:       cuIA58qktP4
soundcloud:    1306662496
aliases:
- /recordings/2022-07-12-rocking-all-over-the-world
artists:
- voice: Vocals
  name: David
- voice: Guitar
  name: Rohith
- voice: Bass
  name: Rainer
- voice: Drums
  name: Fabian
---
The first recording we did. And not everything went good.  
But we got some insights for the next times.

### Retrospective

- The Video Setup needs to be made better
  - it was not clear how to record
  - The Frame Rate is to low
- Reverb for the vocals during recording
- Access to the Mixer did not work as the app assumed to be offline
- The DI were not working
- Having the meetup information in two languages is some effort

### Song/Ideas for the next round

- [**Live - Lightning Crashes**][llc] ([recording](/meetup-recordings/2022-08-09-lightning-crashes/))
- [Foo Fighters - Times Like These][fftlt]

[llc]: https://www.youtube.com/watch?v=xsJ4O-nSveg
[fftlt]: https://www.youtube.com/watch?v=rhzmNRtIp8k
