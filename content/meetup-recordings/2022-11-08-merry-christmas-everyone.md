---
title:         Merry Christmas Everyone - Shakin' Stevens
date:          2022-10-17
publishDate:   2022-10-17T00:00:00+0200
recordingDate: 2022-11-08T19:30:00+0200
categories:    meetup live-recording
audioDbId:     33125899
status:        Video
meetup:        https://www.meetup.com/musicloversmeetup-munich/events/289377800/
youtube:       XUxFyh9Hyco
soundcloud:    1391016757
artists:
- voice: Guitar, Vocals, Chimes, Bells
  name: Rohith
- voice: Drums, Percussion, Chimes, Bells
  name: Fabian
- voice: Bass, Vocals, Chimes, Bells
  name: Rainer
---
Yes, it's recorded, and all the audio is mixed. Thanks to Rohith for the great work.

It will still take a while until the video is done.
