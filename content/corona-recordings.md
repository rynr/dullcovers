---
title: Corona Recordings
date:  2022-07-14 21:53:43 +0200
lang:  en
aliases:
- /project/4c21def0-6bfe-4be5-9be0-7530059e2766
- /project/3d548797-1c1d-4850-ba95-a069a68f946c
---
This project started in 2021 during the corona pandemic. We recorded several songs, each track by someone else, and mixed them at the end.  
After there were no further restriction, the project lost traction. The next way to create music is by live recording. Checkout the [Meetup Recoring](/meetup-recordings/)-section.

### Recordings

- [Skyfall - Adele](https://soundcloud.com/dullcovers/skyfall), [chamberorchestra version](https://soundcloud.com/dullcovers/skyfall-cover)
- [Alessandro Marcello - Sonata in E Minor for lo (Op. 1, No. 2)](https://soundcloud.com/dullcovers/marcello-sonata)
- [Panama - Van Halen](https://soundcloud.com/dullcovers/panama)
- [Last Christmas - Wham](https://soundcloud.com/dullcovers/last-christmas)
- [Torn - Natalie Imbrulia](https://soundcloud.com/dullcovers/torn)
- [Wild World - Cat Stevens](https://soundcloud.com/dullcovers/wild-world)
- [Stairway To Heaven - Led Zeppelin](https://soundcloud.com/dullcovers/stairway-to-heaven)
- [Before He Cheats - Carrie Underwood](https://soundcloud.com/dullcovers/before-he-cheats)
- [Dont Speak - No Doubt](https://soundcloud.com/dullcovers/dont-speak)
- [Sober - P!nk](https://soundcloud.com/dullcovers/sober-pink)
- [Blue Moon Of Kentucky](https://soundcloud.com/dullcovers/blue-moon-of-kentucky)
- [Fairytale Song](https://soundcloud.com/dullcovers/fairytale)
- [Its My Life - Bon Jovi](https://soundcloud.com/dullcovers/its-my-life)
- [Crazy Little Thing Called Love - Queen](https://soundcloud.com/dullcovers/crazy-little-thing-called-love)
- [Thrill Is Gone - B.B.  King](https://soundcloud.com/dullcovers/thrill-is-gone)
- [Vertigo - U2](https://soundcloud.com/dullcovers/vertigo)
- [Already Over Me - Rolling Stones](https://soundcloud.com/dullcovers/already-over-me)
- [Perfect 10 - The Beautiful South](https://soundcloud.com/dullcovers/perfect-10)
- [Easy To Love - Slut](https://soundcloud.com/dullcovers/easy-to-love)
- [Yesterday - Beatles/King Singers](https://soundcloud.com/dullcovers/yesterday)
- [Wonderful World - Joey Ramone](https://soundcloud.com/dullcovers/wonderful-world)
- [The Wizard - Uriah Heep](https://soundcloud.com/dullcovers/the-wizard-uriah-heep)
