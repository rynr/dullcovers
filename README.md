DullCovers!
===========

These are the sources for [dullcovers.eu](https://dullcovers.eu).

Local development
-----------------

Install [hugo](https://gohugo.io/), run `hugo server -D` and follow the instructions.
